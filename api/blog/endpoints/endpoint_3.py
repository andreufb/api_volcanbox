import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import logging
from flask_restplus import Resource
from flask import request, jsonify
from api.blog.serializers import endpoint3, input_endpoint3, endpoint3_id
# from api.blog.db_handler import Db_Handler as db
from api.restplus import api
from flask_jwt_extended import jwt_required

log = logging.getLogger(__name__)

name = 'endpoint-3'
Name = 'Endpoint-3'

ns = api.namespace(name, description='Operations related to ' + name)


@ns.route('/')
class EndpointItems(Resource):

    @api.response(205, name + ' not found.')
    @api.response(200, 'Found ' + name + '.')
    # @jwt_required
    def get(self):
        """
        Description get all info.
        """
        try:
            return True, 200
        except:
            return "Not found " + name + '.', 205


    @api.marshal_with(endpoint3)
    @api.expect(endpoint3_id)
    @api.response(400, 'Bad request.')
    @api.response(204, name + ' not found.')
    @api.response(200, 'Found ' + name +'.')
    # @jwt_required
    def post(self):
        """
        Description post.
        """
        try:
            return True, 200
        except:
            return None, 400


    @api.expect(input_endpoint3)
    @api.response(405, 'Could not append new ' + name + '.')
    @api.response(201, Name + ' configured.')
    # @jwt_required
    def put(self):
        """
        Description put.
        """
        try:
            return ("Configured " + name), 201
        except:
            return "The " + name + " could not be configured, revise that all required camps are dispatched.", 405





@ns.route('/<id>')
@api.response(204, Name + 'not found.')
class EndpointItemsGet(Resource):
    @api.marshal_with(endpoint3)
    @api.response(200, 'Found ' + name + '.')
    # @jwt_required
    def get(self, id):
        """
        Description get endpoint.
        """
        try:
            return True, 200
        except:
            return "Could not find the " + name + ", revise the id value.", 204


    # @api.marshal_with(endpoint3)
    @api.response(201, 'Deleted ' + name + '.')
    @api.response(205, Name + 'can not be deleted.')
    # @jwt_required
    def delete(self, id):
        """
        Description delete endpoint.
        """
        try:
            return True, 201
        except:
            return "Could not delete the " + name + ", revise the id value.", 204