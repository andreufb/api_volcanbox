import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
from flask_restplus import fields
from api.restplus import api


pagination = api.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

# Authorization
credentials = api.model('Credentials', {
    'email': fields.String(description='email to acces'),
    'password': fields.String(description='password')
})

auth_response = api.model('Response', {
    "token": fields.String(description='Token or error message')
})


# Endpoint 1
endpoint1 = api.model('Endpoint', {
    'idendpoint1': fields.Integer(readOnly=True, description='The unique identifier of a endpoint'),
    'label': fields.String(description='label of a endpoint'),
    'description': fields.String(description='Description of a endpoint')
})

input_endpoint1 = api.model('Endpoint', {
    'label': fields.String(description='label of a endpoint'),
    'description': fields.String(description='Description of a endpoint')
})

endpoint1_id = api.model('Endpoint id', {
    "idendpoint1": fields.Integer(description='Unique identifier of a endpoint')
})


# Endpoint 2
endpoint2 = api.model('Endpoint', {
    'idendpoint2': fields.Integer(readOnly=True, description='The unique identifier of a endpoint'),
    'label': fields.String(description='label of a endpoint'),
    'description': fields.String(description='Description of a endpoint')
})

input_endpoint2 = api.model('Endpoint', {
    'label': fields.String(description='label of a endopint'),
    'description': fields.String(description='Description of a endpoint')
})

endpoint2_id = api.model('Endpoint id', {
    "idendpoint2": fields.Integer(description='Unique identifier of a endpoint')
})


# Endpoint 3
endpoint3 = api.model('Endpoint', {
    'idendpoint3': fields.Integer(readOnly=True, description='The unique identifier of a endpoint'),
    'label': fields.String(description='label of a endpoint'),
    'description': fields.String(description='Description of a endpoint')
})

input_endpoint3 = api.model('Endpoint', {
    'label': fields.String(description='label of a endopint'),
    'description': fields.String(description='Description of a endpoint')
})

endpoint3_id = api.model('Endpoint id', {
    "idendpoint3": fields.Integer(description='Unique identifier of a endpoint')
})

# Endpoint 4
endpoint4 = api.model('Endpoint', {
    'idendpoint4': fields.Integer(readOnly=True, description='The unique identifier of a endpoint'),
    'label': fields.String(description='label of a endpoint'),
    'description': fields.String(description='Description of a endpoint')
})

input_endpoint4 = api.model('Endpoint', {
    'label': fields.String(description='label of a endopint'),
    'description': fields.String(description='Description of a endpoint')
})

endpoint4_id = api.model('Endpoint id', {
    "idendpoint4": fields.Integer(description='Unique identifier of a endpoint')
})


# Endpoint 5
endpoint5 = api.model('Endpoint', {
    'idendpoint5': fields.Integer(readOnly=True, description='The unique identifier of a endpoint'),
    'label': fields.String(description='label of a endpoint'),
    'description': fields.String(description='Description of a endpoint')
})

input_endpoint5 = api.model('Endpoint', {
    'label': fields.String(description='label of a endopint'),
    'description': fields.String(description='Description of a endpoint')
})

endpoint5_id = api.model('Endpoint id', {
    "idendpoint": fields.Integer(description='Unique identifier of a endpoint')
})